clear ;
close all ;

%% Model parameters 
eps = 0.1 ; 
Lx = 90.0 ; 
Ly = 90.0 ;
g = 1.0 ;

%% Simulation details 
gptsx = 100 ;
gptsy = 100 ;
t = 140.0 ;
nts = 7000 ;
qc = 1 ;

%% Run simulation
 [finsol,initsol,mvie] = shfun2d(eps,Lx,Ly,g,qc,gptsx,gptsy,t,nts) ;

v = VideoWriter('Hexagons.avi');
v.Quality  = 100 ;
open(v) ;
writeVideo(v,mvie(:,1)) ;
close(v) ;



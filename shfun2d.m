function [finsol,initsol,mvie] = shfun2d(eps,Lx,Ly,g,qc,gptsx,gptsy,t,nts) 


%% Spatial and temporal discretization
dx = Lx/(gptsx-1) ;
dy = Ly/(gptsy-1) ;
dt = t/nts ;

x = linspace(0,Lx,gptsx)' ;
y = linspace(0,Ly,gptsy)' ;
[xfin,yfin] = meshgrid(x,y) ;
%% Construct the LHS matrix
dmat = [] ;

for i=1:gptsy
    for j=1:gptsx
 
        
    if(j==1 || j==gptsx)
        indxl = gptsx-1;
        indxr = 2 ;
    else
        indxl = j-1 ;
        indxr = j+1 ;
         
    end
    
         
    if(i==1 || i==gptsy)
        indxu = gptsy-1 ;
        indxd = 2 ;
    else
        indxu = i-1 ;
        indxd = i+1 ;
    end
    
     mnindx = (i-1)*gptsx + j ;
     cvindl = (i-1)*gptsx + indxl ;
     cvindr = (i-1)*gptsx + indxr ;
     cvindu = (indxu-1)*gptsx + j ;
     cvindd = (indxd-1)*gptsx + j ;
    
   dmat = [dmat ; [mnindx,mnindx+gptsx*gptsy,1]] ;
   dmat = [dmat ; [mnindx,mnindx,2/(dx*dx) + 2/(dy*dy)]] ;
   dmat = [dmat ; [mnindx,cvindl,-1/(dx*dx)]] ;
   dmat = [dmat ; [mnindx,cvindr,-1/(dx*dx)]] ;
   dmat = [dmat ; [mnindx,cvindu,-1/(dy*dy)]] ;
   dmat = [dmat ; [mnindx,cvindd,-1/(dy*dy)]] ;
  
   end
end


 for i=1:gptsy
    for j=1:gptsx
        
      if(j==1 || j==gptsx)
        indxl = gptsx-1;
        indxr = 2 ;
    else
        indxl = j-1 ;
        indxr = j+1 ;
         
    end
    
         
    if(i==1 || i==gptsy)
        indxu = gptsy-1 ;
        indxd = 2 ;
    else
        indxu = i-1 ;
        indxd = i+1 ;
    end
    
     mnindx = (i-1)*gptsx + j ;
     cvindl = (i-1)*gptsx + indxl ;
     cvindr = (i-1)*gptsx + indxr ;
     cvindu = (indxu-1)*gptsx + j ;
     cvindd = (indxd-1)*gptsx + j ;  
        
   dmat = [dmat ; [mnindx+gptsx*gptsy,mnindx,3/(2*dt) - (eps - qc*qc*qc*qc)]] ;
   dmat = [dmat ; [mnindx+gptsx*gptsy,mnindx+gptsx*gptsy,2*qc*qc-2/(dx*dx)-2/(dy*dy)]] ;
   dmat = [dmat ; [mnindx+gptsx*gptsy,cvindl+gptsx*gptsy,1/(dx*dx)]] ;
   dmat = [dmat ; [mnindx+gptsx*gptsy,cvindr+gptsx*gptsy,1/(dx*dx)]] ;
   dmat = [dmat ; [mnindx+gptsx*gptsy,cvindu+gptsx*gptsy,1/(dy*dy)]] ;
   dmat = [dmat ; [mnindx+gptsx*gptsy,cvindd+gptsx*gptsy,1/(dy*dy)]] ;

    end
 end

dmat = sparse(dmat(:,1),dmat(:,2),dmat(:,3),2*gptsy*gptsx,2*gptsy*gptsx) ;
%dmat = inv(dmat) ;
tic ;
dmat = dmat\speye(2*gptsx*gptsy) ;
toc ;
%% RHS

% Initial conditions 
u0 = zeros(gptsy,gptsx) ;
urand = 5.0e-5*rand(gptsy,gptsx) ;
u0 = u0 + urand ;
u0(1,:) = u0(end,:) ;
u0(:,1) = u0(:,end) ;
umin1 = zeros(gptsx*gptsy,1) ;
initsol = reshape(umin1,gptsx,gptsy)' ;
surf(xfin,yfin,initsol) ;
% Smoothen the color variation
shading interp ;
% Choose the colormap
colormap bone ;
% Change the view to a vertical view so that the 2D projection is visible
% to identify patterns
view(2) ;
mvie(1,1) = getframe(gcf) ;
cla ;
u0 = reshape(u0',gptsx*gptsy,1) ;
bvec = zeros(2*gptsx*gptsy,1) ;

for i=1:nts
    bvec(gptsx*gptsy+1:2*gptsx*gptsy,1) = 2*u0(1:gptsx*gptsy,1)/dt - umin1(1:gptsx*gptsy)/(2*dt) ;
    bvec(gptsx*gptsy+1:2*gptsx*gptsy,1) = bvec(gptsx*gptsy+1:2*gptsx*gptsy,1) + 2*g*u0(1:gptsx*gptsy,1).^2 ;
    bvec(gptsx*gptsy+1:2*gptsx*gptsy,1) = bvec(gptsx*gptsy+1:2*gptsx*gptsy,1) - g*umin1(1:gptsx*gptsy,1).^2 ;
    bvec(gptsx*gptsy+1:2*gptsx*gptsy,1) = bvec(gptsx*gptsy+1:2*gptsx*gptsy,1) - 2*u0(1:gptsx*gptsy,1).^3 ;
    bvec(gptsx*gptsy+1:2*gptsx*gptsy,1) = bvec(gptsx*gptsy+1:2*gptsx*gptsy,1) + umin1(1:gptsx*gptsy,1).^3 ;
    if(i==1)
        tic ;
    end
    solvec = dmat*bvec ;
    if(i==1)
        toc ;
    end
    umin1 = u0 ;
    u0 = solvec(1:gptsx*gptsy,1) ;
    soltim = reshape(umin1,gptsx,gptsy)' ;
    surf(xfin,yfin,soltim) ;
    % Smoothen the color variation
    shading interp ;
    % Choose the colormap
    colormap bone;
    % Change the view to a vertical view so that the 2D projection is visible
    % to identify patterns
    view(2) ;
    mvie(i+1,1) = getframe(gcf) ;
    cla ;
end

finsol = reshape(u0,gptsx,gptsy)' ;
surf(xfin,yfin,finsol) ;
% Smoothen the color variation
shading interp ;
% Choose the colormap
colormap bone;
% Change the view to a vertical view so that the 2D projection is visible
% to identify patterns
view(2) ;
mvie(i+1,1) = getframe(gcf) ;
cla ;

end